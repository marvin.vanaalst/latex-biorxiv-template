# biorxiv latex template

[![pipeline status](https://gitlab.com/marvin.vanaalst/latex-biorxiv-template/badges/main/pipeline.svg)](https://gitlab.com/marvin.vanaalst/latex-biorxiv-template/-/commits/main)
[![download pdf](https://img.shields.io/badge/Download-PDF-green)](https://gitlab.com/marvin.vanaalst/latex-biorxiv-template/-/jobs/artifacts/main/file/tex/main.pdf?job=build_paper)
