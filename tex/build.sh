#!/bin/bash
# expects input like "main.tex"

NAME=$(echo $1$ | (cut -d "." -f 1))

pdflatex -synctex=1 -interaction=nonstopmode -file-line-error --shell-escape "$1" || echo "failed, but continue running"
biber $NAME  || echo "failed, but continue running"
pdflatex -synctex=1 -interaction=nonstopmode -file-line-error --shell-escape "$1" || echo "failed, but continue running"
pdflatex -synctex=1 -interaction=nonstopmode -file-line-error --shell-escape "$1" || echo "failed, but continue running"

